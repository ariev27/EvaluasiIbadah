package com.ercode.bhegenk.ibadah.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ercode.bhegenk.ibadah.R;
import com.ercode.bhegenk.ibadah.models.ModelAktifitas;

import java.util.ArrayList;

/**
 * Created by Ariev27 on 1/2/17
 * Ariev27@live.com
 */

public class AktifitasAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ModelAktifitas> data;
    private Context c;

    public AktifitasAdapter(Context c, ArrayList<ModelAktifitas> data) {
        this.data = data;
        this.c = c;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.list_aktifitas, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyViewHolder mholder = (MyViewHolder) holder;
        mholder.tvNamaAktifitas.setText(data.get(position).getNama());
        mholder.tvSatuan.setText(data.get(position).getSatuan());

        if (data.get(position).getJenis() == 1){
            mholder.txtIsian.setVisibility(View.GONE);
            mholder.tvSatuan.setVisibility(View.GONE);
            mholder.cbIsian.setVisibility(View.VISIBLE);
            mholder.cbIsian.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                }
            });
        } else if (data.get(position).getJenis() == 2){
            mholder.txtIsian.setVisibility(View.VISIBLE);
            mholder.tvSatuan.setVisibility(View.VISIBLE);
            mholder.cbIsian.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    //sisipkan view holder
    private class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvNamaAktifitas, tvSatuan;
        CheckBox cbIsian;
        EditText txtIsian;
        RelativeLayout aktifitasContainer;

        MyViewHolder(View v) {
            super(v);
            tvNamaAktifitas = (TextView) v.findViewById(R.id.tvNamaAktifitas);
            tvSatuan = (TextView) v.findViewById(R.id.tvSatuan);
            cbIsian = (CheckBox) v.findViewById(R.id.cbIsian);
            txtIsian = (EditText) v.findViewById(R.id.txtIsian);
            aktifitasContainer = (RelativeLayout) v.findViewById(R.id.aktifitasContainer);
        }
    }
}