package com.ercode.bhegenk.ibadah.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ercode.bhegenk.ibadah.EditAktifitasActivity;
import com.ercode.bhegenk.ibadah.R;
import com.ercode.bhegenk.ibadah.UpdateAktifitasActivity;
import com.ercode.bhegenk.ibadah.helpers.Constant;
import com.ercode.bhegenk.ibadah.helpers.RbHelper;
import com.ercode.bhegenk.ibadah.helpers.RealmHelper;
import com.ercode.bhegenk.ibadah.models.ModelAktifitas;

import java.util.ArrayList;

/**
 * Created by Ariev27 on 12/22/16
 * Ariev27@live.com
 */

public class EditAktifitasAdapter extends RecyclerView.Adapter<EditAktifitasAdapter.ViewHolder> {
    private ArrayList<ModelAktifitas> data;
    //private ModelAktifitas modelAktifitas;
    private Context c;
    private RealmHelper helper;
//    private int pWaktu;

    public EditAktifitasAdapter(Context c, ArrayList<ModelAktifitas> data) {
        this.data = data;
        this.c = c;
        helper = new RealmHelper(this.c);
    }

    @Override
    public EditAktifitasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_edit_aktifitas, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        //holder.click(data.get(position), holder.aktifitasContainer, listener);
        holder.tvNama.setText(data.get(position).getNama());
        final ModelAktifitas m = data.get(position);

        holder.aktifitasContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(c, UpdateAktifitasActivity.class);
                i.putExtra(Constant.ID, m.getId());
                RbHelper.pre("adapt id " + m.getId());
                i.putExtra(Constant.NAMA, m.getNama());
                i.putExtra(Constant.JENIS, m.getJenis());
                i.putExtra(Constant.SATUAN, m.getSatuan());
                i.putExtra(Constant.TARGET, m.getTarget());
                i.putExtra(Constant.WAKTU, m.getWaktu());
                c.startActivity(i);
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogz = new AlertDialog.Builder(c);
                dialogz.setMessage("Anda yakin mau ngenghapus " + m.getNama() + " ?");
                dialogz.setTitle("Konfirmasi");
                dialogz.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        RbHelper.pre("idnya " + m.getId());
                        helper.hapusAktifitas(m.getId(), m.getNama());
                        Intent i = new Intent(c, EditAktifitasActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        c.startActivity(i);

                    }
                });
                dialogz.setNegativeButton("Tidak",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogx, int id) {
                                dialogx.cancel();
                            }
                        });
                dialogz.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNama;
        ImageButton btnDelete;
        RelativeLayout aktifitasContainer;

        ViewHolder(View itemView) {
            super(itemView);
            tvNama = (TextView) itemView.findViewById(R.id.tvNamaAktifitas);
            btnDelete = (ImageButton) itemView.findViewById(R.id.btnDelete);
            aktifitasContainer = (RelativeLayout) itemView.findViewById(R.id.aktifitasContainer);
        }
    }
}