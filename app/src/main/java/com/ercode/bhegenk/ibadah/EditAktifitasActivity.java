package com.ercode.bhegenk.ibadah;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ercode.bhegenk.ibadah.Adapters.EditAktifitasAdapter;
import com.ercode.bhegenk.ibadah.base.BaseActivity;
import com.ercode.bhegenk.ibadah.helpers.RealmHelper;
import com.ercode.bhegenk.ibadah.models.ModelAktifitas;
import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

public class EditAktifitasActivity extends BaseActivity {
    private ArrayList<ModelAktifitas> data;
    private RealmHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_aktifitas);

        drawerMenu(3);
        helper = new RealmHelper(EditAktifitasActivity.this);
        setupView();
    }

    private void setupView() {
        FloatingActionButton btnTambah = (FloatingActionButton) findViewById(R.id.btnTambah);
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(c, TambahAktifitasActivity.class));
            }
        });

        RecyclerView rvAktifitas = (RecyclerView) findViewById(R.id.rvAktifitas);
        rvAktifitas.setLayoutManager(new LinearLayoutManager(c));
        data = new ArrayList<>();
        try {
            data = helper.findAllAktifitas();
        } catch (Exception e) {
            e.printStackTrace();
        }
        EditAktifitasAdapter adapter = new EditAktifitasAdapter(c, data);
        rvAktifitas.setAdapter(adapter);
    }

    /*public void setRecyclerView() {
        try {
            data = helper.findAllAktifitas();
        } catch (Exception e) {
            e.printStackTrace();
        }
        AktifitasAdapter adapter = new AktifitasAdapter(data, new AktifitasAdapter.OnItemClickListener() {
            @Override
            public void onClick(ModelAktifitas item, RelativeLayout aktifitasContainer) {
                Intent i = new Intent(getApplicationContext(), UpdateAktifitasActivity.class);
                i.putExtra("id", item.getId());
                i.putExtra("nama", item.getNama());
                i.putExtra("satuan", item.getSatuan());
                i.putExtra("target", item.getTarget());
                startActivity(i);
                Toast.makeText(c, "Test", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void delete(ModelAktifitas modelAktifitas, ImageButton btnDelete) {
                dialogDelete(modelAktifitas);
            }
        });
        rvAktifitas.setAdapter(adapter);
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        try {
            data = helper.findAllAktifitas();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //data = helper.findAllAktifitas();
        //setRecyclerView();
    }

}