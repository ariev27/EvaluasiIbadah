package com.ercode.bhegenk.ibadah;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ercode.bhegenk.ibadah.Adapters.AktifitasAdapter;
import com.ercode.bhegenk.ibadah.Adapters.EditAktifitasAdapter;
import com.ercode.bhegenk.ibadah.base.BaseActivity;
import com.ercode.bhegenk.ibadah.helpers.RbHelper;
import com.ercode.bhegenk.ibadah.helpers.RealmHelper;
import com.ercode.bhegenk.ibadah.models.ModelAktifitas;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends BaseActivity {
    private RealmHelper helper;
    private TextView tvTglSkr;
    private ImageButton btnSimpan;
    private RecyclerView rvData;
    private ArrayList<ModelAktifitas> data;
    private HashMap<Long, Integer> dataSimpan = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerMenu(1);
        helper = new RealmHelper(MainActivity.this);
        setupView();

    }

    private void setupView() {

        tvTglSkr = (TextView) findViewById(R.id.tvTglSkr);
        rvData = (RecyclerView) findViewById(R.id.rvData);
        btnSimpan = (ImageButton) findViewById(R.id.btnSimpan);

        tvTglSkr.setText(RbHelper.tglSekarang1());
        rvData.setLayoutManager(new LinearLayoutManager(c));
        data = new ArrayList<>();
        try {
            data = helper.findAllAktifitas();
        } catch (Exception e) {
            e.printStackTrace();
        }
        AktifitasAdapterx adapter = new AktifitasAdapterx(c, data);
        rvData.setAdapter(adapter);
    }

    private class AktifitasAdapterx extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<ModelAktifitas> data;
        private Context c;

        public AktifitasAdapterx(Context c, ArrayList<ModelAktifitas> data) {
            this.data = data;
            this.c = c;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(c).inflate(R.layout.list_aktifitas, parent, false);
            return new MyViewHolder(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            MyViewHolder mholder = (MyViewHolder) holder;
            final ModelAktifitas a = data.get(position);
            mholder.tvNamaAktifitas.setText(a.getNama());
            mholder.tvSatuan.setText(a.getSatuan());

            dataSimpan.put(a.getId(), 0);

            if (a.getJenis() == 1){
                mholder.txtIsian.setVisibility(View.GONE);
                mholder.tvSatuan.setVisibility(View.GONE);
                mholder.cbIsian.setVisibility(View.VISIBLE);
                mholder.cbIsian.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b){
                            dataSimpan.put(a.getId(), 1);
                        }else{
                            dataSimpan.put(a.getId(), 0);
                        }
                    }
                });
            } else if (data.get(position).getJenis() == 2){
                mholder.txtIsian.setVisibility(View.VISIBLE);
                mholder.tvSatuan.setVisibility(View.VISIBLE);
                mholder.cbIsian.setVisibility(View.GONE);

                mholder.txtIsian.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (editable.length() > 0){
                            dataSimpan.put(a.getId(), Integer.parseInt(editable.toString()));
                        }else{
                            dataSimpan.put(a.getId(), 0);
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return data == null ? 0 : data.size();
        }

        //sisipkan view holder
        private class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tvNamaAktifitas, tvSatuan;
            CheckBox cbIsian;
            EditText txtIsian;
            RelativeLayout aktifitasContainer;

            MyViewHolder(View v) {
                super(v);
                tvNamaAktifitas = (TextView) v.findViewById(R.id.tvNamaAktifitas);
                tvSatuan = (TextView) v.findViewById(R.id.tvSatuan);
                cbIsian = (CheckBox) v.findViewById(R.id.cbIsian);
                txtIsian = (EditText) v.findViewById(R.id.txtIsian);
                aktifitasContainer = (RelativeLayout) v.findViewById(R.id.aktifitasContainer);
            }
        }
    }

    private void actionSimpan() {

    }
}
