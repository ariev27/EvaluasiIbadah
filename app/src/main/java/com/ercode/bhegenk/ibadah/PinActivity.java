package com.ercode.bhegenk.ibadah;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.ercode.bhegenk.ibadah.base.BaseActivity;
import com.ercode.bhegenk.ibadah.helpers.RbHelper;

public class PinActivity extends BaseActivity {
    private TextView tvPetunjuk;
    private Button btnOke;
    private CheckBox cbTampil;
    private EditText txtPin;
    private SharedPreferences setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        setupView();
    }

    private void setupView() {
        txtPin = (EditText) findViewById(R.id.txtPin);
        btnOke = (Button) findViewById(R.id.btnOke);
        tvPetunjuk = (TextView) findViewById(R.id.tvPetunjuk);

        setting = PreferenceManager.getDefaultSharedPreferences(c);
        String petunjuk = setting.getString("petunjuk", "");

        tvPetunjuk.setText(petunjuk);
        RbHelper.pre("petunjuk " + petunjuk);

        btnOke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifikasiPin(txtPin);
            }
        });
        cbTampil = (CheckBox) findViewById(R.id.cbTampil);
        cbTampil.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (!isChecked) {
                    // show password
                    txtPin.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    txtPin.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
    }

    private boolean verifikasiPin(EditText editText) {
        boolean cancel = false;
        View focusView = null;
        SharedPreferences setting = PreferenceManager.getDefaultSharedPreferences(c);
        int pin = Integer.parseInt(setting.getString("pin", "defaultValue"));
        if (RbHelper.isEmpty(txtPin)) {
            txtPin.setError("PIN harus di isi !!");
            focusView = txtPin;
            cancel = true;
        } else if (Integer.parseInt(editText.getText().toString()) == pin) {
            startActivity(new Intent(c, MainActivity.class));
            finish();
        } else {
            txtPin.setError("PIN salah !!");
            focusView = txtPin;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        }
        return cancel;
    }

}
