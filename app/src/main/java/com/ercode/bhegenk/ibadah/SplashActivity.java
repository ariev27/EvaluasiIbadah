package com.ercode.bhegenk.ibadah;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;

import com.ercode.bhegenk.ibadah.base.BaseActivity;
import com.ercode.bhegenk.ibadah.helpers.RbHelper;

public class SplashActivity extends BaseActivity {
    private SharedPreferences setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();

            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        setContentView(R.layout.activity_splash);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                setting = PreferenceManager.getDefaultSharedPreferences(c);
                String pin = setting.getString("pin", "");
                RbHelper.pre("pinnya " + pin);

                if (pin.length() > 0) {
                    startActivity(new Intent(c, PinActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(c, MainActivity.class));
                    finish();
                }
            }
        }, 2000L);
    }
}