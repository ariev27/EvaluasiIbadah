package com.ercode.bhegenk.ibadah;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.ercode.bhegenk.ibadah.base.BaseActivity;
import com.ercode.bhegenk.ibadah.helpers.RbHelper;
import com.ercode.bhegenk.ibadah.helpers.RealmHelper;
import com.ercode.bhegenk.ibadah.models.ModelAktifitas;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

public class TambahAktifitasActivity extends BaseActivity {
    private EditText txtNamaAktifitas, txtSatuan, txtTarget;
    private RadioButton radioIya, radioIsian;
    private RadioGroup radioGroup;
    private Spinner spWaktu;
    private ModelAktifitas model;
    private long id = 1;
    private int jenis, waktu = 0;
    private String namaAktifitas, satuan, target, intentPosition, intentNamaAktifitas,
            intentSatuan, intentTarget, intentWaktu;
    private RealmHelper realmHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_aktifitas);

        realmHelper = new RealmHelper(TambahAktifitasActivity.this);
        setupView();
    }

    private void setupView() {
        txtNamaAktifitas = (EditText) findViewById(R.id.txtNamaAktifitas);
        txtSatuan = (EditText) findViewById(R.id.txtSatuan);
        txtTarget = (EditText) findViewById(R.id.txtTarget);
        radioIya = (RadioButton) findViewById(R.id.radioIya);
        radioIsian = (RadioButton) findViewById(R.id.radioIsian);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        ImageButton btnSimpan = (ImageButton) findViewById(R.id.btnSimpan);
        spWaktu = (Spinner) findViewById(R.id.spWaktu);

        List<String> spList = new ArrayList<>();
        spList.add("Harian");
        spList.add("Mingguan");
        spList.add("Bulanan");
        ArrayAdapter<String> spAdapter = new ArrayAdapter<>(c, R.layout.spinner_item, spList);
        //spAdapter.setDropDownViewResource(c, R.layout.spinner_item);
        spWaktu.setAdapter(spAdapter);
        spWaktu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                waktu = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        txtSatuan.setVisibility(GONE);
        radioIya.setChecked(true);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioIsian.isChecked()) {
                    txtSatuan.setVisibility(View.VISIBLE);
                } else if (radioIya.isChecked()) {
                    txtSatuan.setVisibility(View.GONE);
                }
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionSimpan();
            }
        });
    }

    private void actionSimpan() {
        boolean cancel = false;
        View focusView = null;

        if (RbHelper.isEmpty(txtNamaAktifitas)) {
            txtNamaAktifitas.setError("Nama aktifitas harus di isi !!");
            focusView = txtNamaAktifitas;
            cancel = true;
        } else if (RbHelper.isEmpty(txtTarget)) {
            txtTarget.setError("Target harus di isi !!");
            focusView = txtTarget;
            cancel = true;
        } else if (radioIsian.isChecked()) {
            if (RbHelper.isEmpty(txtSatuan)) {
                txtSatuan.setError("Satuan harus di isi !!");
                focusView = txtSatuan;
                cancel = true;
            }
        }
        if (cancel) {
            focusView.requestFocus();
        } else {

            if (radioIya.isChecked()) {
                jenis = 1;
            } else if (radioIsian.isChecked()) {
                jenis = 2;
                satuan = txtSatuan.getText().toString();
            }

            namaAktifitas = txtNamaAktifitas.getText().toString();
            target = txtTarget.getText().toString();

            realmHelper.tambahAktifitas(namaAktifitas, jenis, satuan, target, waktu);
            //id++;

            finish();
            Intent i = new Intent(getBaseContext(), EditAktifitasActivity.class);
            startActivity(i);
            finish();
        }
    }
}