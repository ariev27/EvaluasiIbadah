package com.ercode.bhegenk.ibadah;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.ercode.bhegenk.ibadah.base.BaseActivity;
import com.ercode.bhegenk.ibadah.helpers.Constant;
import com.ercode.bhegenk.ibadah.helpers.RbHelper;
import com.ercode.bhegenk.ibadah.helpers.RealmHelper;

import java.util.ArrayList;
import java.util.List;

public class UpdateAktifitasActivity extends BaseActivity {
    private EditText txtNamaAktifitas, txtSatuan, txtTarget;
    private RadioButton radioIya, radioIsian;
    private RadioGroup radioGroup;
    private ImageButton btnUpdate;
    private Spinner spWaktu;
    private ArrayAdapter<String> spAdapter;
    private List<String> spList;
    private String namaAktifitas;
    private int jenis;
    private String satuan;
    private String target;
    private int waktu;
    private String intentNamaAktifitas;
    private int intentJenis;
    private String intentSatuan = null;
    private int intentWaktu;
    private String intentTarget = null;
    private long intentId;
    private RealmHelper realmHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_aktifitas);

        realmHelper = new RealmHelper(UpdateAktifitasActivity.this);
        setupView();
        getData();
    }

    private void setupView() {
        txtNamaAktifitas = (EditText) findViewById(R.id.txtNamaAktifitas);
        txtSatuan = (EditText) findViewById(R.id.txtSatuan);
        txtTarget = (EditText) findViewById(R.id.txtTarget);
        radioIya = (RadioButton) findViewById(R.id.radioIya);
        radioIsian = (RadioButton) findViewById(R.id.radioIsian);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        btnUpdate = (ImageButton) findViewById(R.id.btnUpdate);
        spWaktu = (Spinner) findViewById(R.id.spWaktu);
        spWaktu.setSelection(intentWaktu);

        spList = new ArrayList<>();
        spList.add("Harian");
        spList.add("Mingguan");
        spList.add("Bulanan");
        spAdapter = new ArrayAdapter<>(c, R.layout.spinner_item, spList);
        spWaktu.setAdapter(spAdapter);
        spWaktu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                waktu = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        txtSatuan.setVisibility(View.GONE);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean cancel = false;
            View focusView = null;

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioIsian.isChecked()) {
                    txtSatuan.setVisibility(View.VISIBLE);
                    txtSatuan.setText(intentSatuan);
                    radioIsian.setSelected(true);
                    if (RbHelper.isEmpty(txtSatuan)) {
                        txtSatuan.setError("Satuan harus di isi !!");
                        focusView = txtSatuan;
                        cancel = true;
                    }

                } else if (radioIya.isChecked()) {
                    txtSatuan.setVisibility(View.GONE);
                    radioIya.setSelected(true);
                }
            }
        });

        spWaktu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                waktu = spWaktu.getSelectedItemPosition();
                RbHelper.pre("Selected " + waktu);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionUpdate();
            }
        });

    }

    private void getData() {
        intentId = getIntent().getLongExtra(Constant.ID, 0);
        RbHelper.pre("intn id " + intentId);

        intentNamaAktifitas = getIntent().getStringExtra(Constant.NAMA);
        RbHelper.pre("intn nama " + intentNamaAktifitas);

        intentJenis = getIntent().getIntExtra(Constant.JENIS, 0);
        RbHelper.pre("intn jenis " + intentJenis);

        intentSatuan = getIntent().getStringExtra(Constant.SATUAN);
        RbHelper.pre("intn satuan " + intentSatuan);

        intentTarget = getIntent().getStringExtra(Constant.TARGET);
        RbHelper.pre("intn target " + intentTarget);

        intentWaktu = getIntent().getIntExtra(Constant.WAKTU, 0);
        RbHelper.pre("intn waktu " + intentWaktu);

        txtNamaAktifitas.setText(intentNamaAktifitas);
        txtTarget.setText(intentTarget);
        spWaktu.setSelection(intentWaktu);
        if (intentJenis == 2) {
            txtSatuan.setVisibility(View.VISIBLE);
            radioIsian.setChecked(true);
        } else if (intentJenis == 1) {
            radioIya.setChecked(true);
        }
    }

    private void actionUpdate() {
        boolean cancel = false;
        View focusView = null;

        if (RbHelper.isEmpty(txtNamaAktifitas)) {
            txtNamaAktifitas.setError("Nama aktifitas harus di isi !!");
            focusView = txtNamaAktifitas;
            cancel = true;
        } else if (RbHelper.isEmpty(txtTarget)) {
            txtTarget.setError("Target harus di isi !!");
            focusView = txtTarget;
            cancel = true;
        } else if (radioIsian.isChecked()) {
            if (RbHelper.isEmpty(txtSatuan)) {
                txtSatuan.setError("Satuan harus di isi !!");
                focusView = txtSatuan;
                cancel = true;
            }
        }
        if (cancel) {
            focusView.requestFocus();
        } else {

            if (radioIya.isChecked()) {
                jenis = 1;
            } else if (radioIsian.isChecked()) {
                jenis = 2;
                satuan = txtSatuan.getText().toString();
            }

            namaAktifitas = txtNamaAktifitas.getText().toString();
            satuan = txtSatuan.getText().toString();
            target = txtTarget.getText().toString();

            realmHelper.updateAktifitas(intentId, namaAktifitas, jenis, satuan, target, waktu);

            finish();
            startActivity(new Intent(c, EditAktifitasActivity.class));
        }
    }
}