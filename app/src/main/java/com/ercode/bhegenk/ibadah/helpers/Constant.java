package com.ercode.bhegenk.ibadah.helpers;

/**
 * Created by Ariev27 on 12/30/16
 * Ariev27@live.com
 */

public class Constant {
    public static final String ID = "id";
    public static final String TARGET = "target";
    public static final String NAMA = "nama";
    public static final String JENIS = "jenis";
    public static final String SATUAN = "satuan";
    public static final String WAKTU = "waktu";
}
