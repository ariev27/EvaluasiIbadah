package com.ercode.bhegenk.ibadah.helpers;

import android.content.Context;

import com.ercode.bhegenk.ibadah.models.Aktifitas;
import com.ercode.bhegenk.ibadah.models.ModelAktifitas;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


public class RealmHelper {
    private Realm realm;
    private Context context;

    /**
     * constructor untuk membuat instance realm
     *
     * @param context
     */
    public RealmHelper(Context context) {
        realm = Realm.getInstance(context);
        this.context = context;
    }

    /**
     * menambah data
     *
     * @param nama
     * @param satuan
     * @param target
     */
    public void tambahAktifitas(String nama, int jenis, String satuan, String target, int waktu) {
        //id++;
        long id = 0;
        Number currentId = realm.where(Aktifitas.class).max("id");
        if (currentId == null) {
            id = 1;
        } else {
            id = currentId.longValue() + 1;
        }

        RbHelper.pre("id11 " + id);
        realm.beginTransaction();
        Aktifitas aktifitas = new Aktifitas();
        aktifitas.setId(id);
        aktifitas.setNama(nama);
        aktifitas.setJenis(jenis);
        aktifitas.setSatuan(satuan);
        aktifitas.setTarget(target);
        aktifitas.setWaktu(waktu);

        realm.copyToRealm(aktifitas);
        realm.commitTransaction();
        //id++;

        RbHelper.pre("Added ; " + nama);
        RbHelper.pesan(context, nama + " berhasil disimpan");
    }

    /**
     * method mencari semua aktifitas
     */
    public ArrayList<ModelAktifitas> findAllAktifitas() {
        ArrayList<ModelAktifitas> data = new ArrayList<>();


        RealmResults<Aktifitas> realmResult = realm.where(Aktifitas.class).findAll();
        realmResult.sort("id", Sort.DESCENDING);
        if (realmResult.size() > 0) {
            RbHelper.pre("Size : " + realmResult.size());

            for (int i = 0; i < realmResult.size(); i++) {
                String namaAktifitas, satuan, target;
                int jenis, waktu;
                long id;
                id = realmResult.get(i).getId();
                namaAktifitas = realmResult.get(i).getNama();
                jenis = realmResult.get(i).getJenis();
                satuan = realmResult.get(i).getSatuan();
                target = realmResult.get(i).getTarget();
                waktu = realmResult.get(i).getWaktu();
                data.add(new ModelAktifitas(id, namaAktifitas, jenis, satuan, target, waktu));
            }

        } else {
            RbHelper.pre("Size : 0");
            RbHelper.pesan(context, "Aktifitas Kosong");
        }

        return data;
    }


    /**
     * method update article
     * @param id
     * @param namaAktifitas
     * @param satuan
     * @param target
     */
    public void updateAktifitas(long id, String namaAktifitas, int jenis, String satuan, String target, int waktu) {
        realm.beginTransaction();
        Aktifitas aktifitas = realm.where(Aktifitas.class).equalTo("id", id).findFirst();
        aktifitas.setNama(namaAktifitas);
        aktifitas.setJenis(jenis);
        aktifitas.setSatuan(satuan);
        aktifitas.setTarget(target);
        aktifitas.setWaktu(waktu);
        realm.commitTransaction();
        RbHelper.pre("Updated : " + namaAktifitas);

        RbHelper.pesan(context, namaAktifitas + " berhasil di update");
    }

    /**
     * method menghapus article berdasarkan id
     *
     * @param id
     */
    public void hapusAktifitas(long id, String nama) {
        RealmResults<Aktifitas> dataDesults = realm.where(Aktifitas.class).equalTo("id", id).findAll();
        realm.beginTransaction();
        dataDesults.remove(0);
        dataDesults.removeLast();
        dataDesults.clear();
        realm.commitTransaction();

        RbHelper.pesan(context, "Hapus " + nama + " berhasil");
    }
}