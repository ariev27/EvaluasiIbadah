package com.ercode.bhegenk.ibadah.models;

/**
 * Created by Ariev27 on 12/22/16
 * Ariev27@live.com
 */
public class ModelAktifitas {
    private long id;
    private String nama;
    private int jenis;
    private String satuan;
    private String target;
    private int waktu;

    public ModelAktifitas(long id, String nama, int jenis, String satuan, String target, int waktu) {
        this.id = id;
        this.nama = nama;
        this.jenis = jenis;
        this.satuan = satuan;
        this.target = target;
        this.waktu = waktu;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String namaAktifitas) {
        this.nama = nama;
    }

    public int getJenis() {
        return jenis;
    }

    public void setJenis(int jenis) {
        this.jenis = jenis;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public int getWaktu() {
        return waktu;
    }

    public void setWaktu(int waktu) {
        this.waktu = waktu;
    }
}
